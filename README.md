Graphformer by SymptoticCycles (a.k.a. Yambam)
==============================================
My first Python project: a platformer (with bitmap collision checking). The goal in the end is to make a platformer using arbitrary mathematical shapes and scrap all the PNG's (except for maybe the player and the obstacles).

Features
--------

The platformer mechanics include wall jumping, double jumping, walking up/down ramps and normal jumping of course. :) Black colored parts are converted to grass/dirt in-game, red colored parts become dangerous elements/obstacles.

Screenshots
-----------
![Screenshot 1](screenshots/screenshot001.png)

Dependencies
------------
To run the game you need the pygame module from [pygame.org](pygame.org/download.shtml).
